---
title: Publishing Obsidian.md notes with GitLab Pages
author: Scott Hampton
author_gitlab: shampton
author_twitter: iamshampton
categories: engineering
description: "How to publish your Obsidian.md documents to a GitLab Pages site"
tags: tutorial
featured: no
postType: Technical Marketing
---

[Obsidian.md](https://obsidian.md) is a "knowledge base" application that uses plain text Markdown files stored in a local folder to
organize your notes. The product has been growing in popularity, partly because of how extensible it is. There are a
lot of community built plugins to help users configure the application to support their specific workflow. There are
many people that use Obsidian to write their blog posts. [Obsidian offers a paid service to publish your notes directly](https://obsidian.md/publish)
and is completely compatible with features Obsidian offers. I suggest you support the Obsidian developers if their product
works for you. If you are looking for an alternative way to publish, this blog post provides a tutorial for how to publish your notes using GitLab
Pages.

You can find an Obsidian.md example in [this demonstration project](https://gitlab.com/gitlab-org/frontend/playground/obsidian-and-gitlab-pages-demo)
which deploys [a GitLab Pages site](https://gitlab-org.gitlab.io/frontend/playground/obsidian-and-gitlab-pages-demo/). 

## Introduction to Obsidian.md

[Obsidan.md](https://obsidian.md), at it's core, is an application that helps manage your markdown files. You can download the application
via their site and create a "workspace" folder when you first start the application. When using the application, all of your notes
will be created in the folder you choose as your "workspace".

![Obsidian application](/images/blogimages/obsidian_md.png){: .shadow}

### Workspace file structure

Inside your Obsidian workspace, you can have any number of folders and markdown files. When you open a folder in Obsidian as your "workspace",
Obsidian will automatically add a folder `.obsidian`, which contains your workspace configuration such as application styles and plugins.
A basic workspace file structure could look something like this:

```
.
├── workspace_folder/
│   └── Other pages/
│   │   └── Another page.md
│   └── .obsidian
│   └── index.md
```

`index.md`
```markdown
# Home

This is a basic home page, and a link to another page in my documents.

See [[Another page]] - note that this link uses wikilinks which Obsidian uses to help you easily link to other notes in your workspace.
```

`Other pages/Another page.md`
```markdown
# Another page

This is another page besides the home page.
```

## Generating a static site to host your notes

In order to publish your notes to GitLab Pages, you need to create a static site to show and navigate your notes.
There are several open source tools that generate static sites from Markdown documents. After experimenting
with a few, I found [MkDocs](https://www.mkdocs.org/) to be the easiest and most compatible with Obsidian.

If you would like to use MkDocs locally, you can install it with `pip install mkdocs`
(Python and [pip as package manager](https://pypi.org/project/pip/) are required).
This is not necessary, because in this tutorial we'll utilize GitLab CI pipelines to install MkDocs and build our site.

There are two small steps you need to make in order to get your existing Obsidian notes working with MkDocs.

### File structure

All files that are not your workspace notes will be created outside of your workspace folder. The following folder structure is
how this final demo project is going to look.

```
.
├── wiki/
│   └── .obsidian
│   └── index.md
├── .gitlab-ci.yml
├── mkdocs.yml
└── requirements.txt
```

 - `wiki/` - this is your Obsidian workspace folder
 - `.obsidian` - the application configuration folder Obsidian uses for your workspace. This will not affect the site.
 - `index.md` - MkDocs looks for `index.md` in your workspace folder to use as your site's home page.
 - `.gitlab-ci.yml` - the GitLab CI configuration file used to deploy your site.
 - `mkdocs.yml` - the MkDocs configuration file use to build and customize your site.
 - `requirements.txt` - this file defines the Python package dependencies for MkDocs.

### Basic MkDocs Configuration

You'll need to create a configuration file `mkdocs.yml` for MkDocs to know how you would like your site to look.
Here are the first four lines we need to configure our notes.

```yaml
site_name: My Obsidian Notes
site_url: https://group-name.gitlab.io/repo-name
site_dir: public
docs_dir: ./wiki
```

- `site_name` - is what will be used as the main title for the web site.
- `site_url` - is used as the "canonical URL" of the site. You will need to use [the default URL provided by GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/getting_started_part_one.html#gitlab-pages-default-domain-names) or your custom domain here.
- `site_dir` - GitLab Pages requires HTML source code to be contained in a `public` folder. This setting tells MkDocs to put the generated files in the `public` folder.
- `docs_dir` - this is the relative path to your workspace folder. I like to name mine `wiki` because it's my personal wikipedia. You can name this folder whatever you want.

We'll come back to this configuration file later to add more custom styles to your site.

## Configuring GitLab CI

We need to configure a GitLab CI job to install MkDocs and build the web site based on our Obsidian notes. The following
`.gitlab-ci.yml` file has the basic setup for this:

```yaml
image: python:3.8-slim

pages:
  stage: deploy
  script:
    # Install all of the python packages for mkdocs
    - pip install -r requirements.txt
    # Build the site using mkdocs
    # --strict aborts the build on any warnings
    # --verbose enables verbose output so that it's easier to see what mkdocs is doing
    # neither --strict nor --verbose are necessary if you choose not to use them
    - mkdocs build --strict --verbose
  artifacts:
    paths:
      - public
  only:
    - main
```

This job will only run when a change is made to the default branch (`main` in this case).

### Python Packages

Note the line `pip install -r requirements.txt` in the above `.gitlab-ci.yml` file. This line is installing MkDocs and any
additional plugins you use to customize your site. You'll need to create a `requirements.txt` file for this script to work:

```text
# Documentation static site generator & deployment tool
mkdocs>=1.1.2
```

We'll come back to this `requirements.txt` file to add a couple more packages to customize our site later.

## Customizing your site

One of the benefits of using MkDocs is that it has a lot of extensions you can add on to customize your site. You can
change the theme of the site, which adjusts the colors and layout. You can also add extensions that improve how your
markdown notes are displayed and interacted with on the site.

### Theme

MkDocs includes two built-in themes (`mkdocs` and `readthedocs`), [as documented on their website](https://www.mkdocs.org/user-guide/choosing-your-theme/).
There are also a lot of [community built themes](https://github.com/mkdocs/mkdocs/wiki/MkDocs-Themes) you can search through and choose to use.
My current favorite theme is [Material](https://github.com/mkdocs/mkdocs/wiki/MkDocs-Themes#material-for-mkdocs-). You can install it by adding it our `requirements.txt` and choosing
it as your theme in the `mkdocs.yml` configuration file, or if you are installing it locally you can install it with `pip install mkdocs-material`.

`requirements.txt`
```text
# Material theme
mkdocs-material>=8.1.7
```

`mkdocs.yml`
```yaml
theme:
  name: material
  palette:
    scheme: slate
```

I have chosen the `slate` scheme for the material theme which makes it darker. You can choose more configuration options
based on [their website documentation](https://squidfunk.github.io/mkdocs-material/setup/changing-the-colors/).

### Extensions

MkDocs includes [built-in extensions](https://www.mkdocs.org/user-guide/configuration/#markdown_extensions) that you can add to your `mkdocs.yml` configuration file. The
[Material](https://github.com/mkdocs/mkdocs/wiki/MkDocs-Themes#material-for-mkdocs-) theme package also comes with many more extensions that we can use. Below are some of my favorite
for working with Obsidian:

```yaml
# Extensions
markdown_extensions:
  - footnotes
  - attr_list
  - pymdownx.highlight
  - pymdownx.superfences
  - pymdownx.details
  - pymdownx.magiclink
  - pymdownx.tasklist
  - pymdownx.emoji
  - admonition
  - toc:
    permalink: true
```

- `footnotes` - adds the ability to define inline footnotes, whech are then rendered below all Markdown content of a document. [See documentation here](https://squidfunk.github.io/mkdocs-material/reference/footnotes/).
- `attr_list` - allows you to add HTML attributes and CSS classes to almost every Markdown inline and block-level element with special syntax. [See documentation here](https://squidfunk.github.io/mkdocs-material/setup/extensions/python-markdown/#attribute-lists).
- `pymdownx.highlight` - adds support for syntax highlighting of code blocks. [See documentation here](https://facelessuser.github.io/pymdown-extensions/extensions/highlight/).
- `pymdownx.superfences` - allows for arbitrary nesting of code and content blocks inside each other. [See documentation here](https://facelessuser.github.io/pymdown-extensions/extensions/superfences/).
- `pymdownx.details` - allows for creating collapsible content blocks. [See documentation here](https://facelessuser.github.io/pymdown-extensions/extensions/details/).
- `pymdownx.magiclink` - provides a number of useful link related features such as auto-link HTML and emails. [See documentation here](https://facelessuser.github.io/pymdown-extensions/extensions/magiclink/).
- `pymdownx.tasklist` - adds support for tasklist syntax. [See documentation here](https://facelessuser.github.io/pymdown-extensions/extensions/tasklist/).
- `pymdownx.emoji` - adds support for inserting emoji via simple short names enclosed within colons (`:short_name:`). [See documentation here](https://facelessuser.github.io/pymdown-extensions/extensions/emoji/).
- `admonition` - allows you to create "callouts" in your documentation. [See documentation here](https://squidfunk.github.io/mkdocs-material/reference/admonitions/).
- `toc:permalink` - adds a table of contents to your page based on your markdown document, and ensures each link is a permanent link that can be reused. [See documentation here](https://python-markdown.github.io/extensions/toc/).

### Plugins

MkDocs also has a community of plugins that add more features when building your site. MkDocs includes some plugins by default that you can use in the configuration file, but in order to use community plugins you have to add them to the
`requirements.txt` file to be installed as packages. The following two plugins are ones that I've found useful, but you
can look at [the list of community plugins here](https://github.com/mkdocs/mkdocs/wiki/MkDocs-Plugins):

```yaml
plugins:
  - search
  - roamlinks
```

- `search` - provides a search bar at the top of your site to easily search your documents. [See documentation here](https://www.mkdocs.org/user-guide/configuration/#search).
- `roamlinks` - adds support for Obsidian's wikilinks feature. [See documentation here](https://github.com/Jackiexiao/mkdocs-roamlinks-plugin).

`requirements.txt`
```text
# Wikilinks support
mkdocs-roamlinks-plugin>=0.1.3
```

If installing locally, you can install roamlinks with `pip install mkdocs-roamlinks-plugin`.

## Combining it all together

After all of the above work is done, you should have a file structure that looks like this:

```
.
├── wiki/
│   └── .obsidian
│   └── index.md
├── .gitlab-ci.yml
├── mkdocs.yml
└── requirements.txt
```

Here are the contents of the three main files that you've been editing:

`.gitlab-ci.yml`
```yaml
image: python:3.8-slim

pages:
  stage: deploy
  script:
    - pip install -r requirements.txt
    - mkdocs build --strict --verbose
  artifacts:
    paths:
      - public
  only:
    - main
```

`mkdocs.yml`
```yaml
site_name: My Obsidian Notes
site_url: https://group-name.gitlab.io/repo-name
site_dir: public

theme:
  name: material
  palette:
    scheme: slate

# Extensions
markdown_extensions:
  - footnotes
  - attr_list
  - pymdownx.highlight
  - pymdownx.superfences
  - pymdownx.details
  - pymdownx.magiclink
  - pymdownx.tasklist
  - pymdownx.emoji
  - admonition
  - toc:
    permalink: true

plugins:
  - search
  - roamlinks
```

`requirements.txt`
```text
# Documentation static site generator & deployment tool
mkdocs>=1.1.2

# Material theme
mkdocs-material>=8.1.7

# Wikilinks support
mkdocs-roamlinks-plugin>=0.1.3
```

Now that your files are all finished, the last step is to push your changes to your GitLab repository and wait for your pipeline
to finish. Once finished, you can go to [your default domain provided by GitLab](https://docs.gitlab.com/ee/user/project/pages/getting_started_part_one.html#gitlab-pages-default-domain-names) or you can
[configure GitLab Pages to use a custom domain](https://docs.gitlab.com/ee/administration/pages/index.html).

Here's a screenshot of the demonstration site created in this tutorial:

![Obsidian application](/images/blogimages/obsidian_mkdocs_site.png){: .shadow}

Share your Obsidian.md deployments in the comments.
